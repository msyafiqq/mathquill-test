//
//  ViewController.swift
//  TestMathQuill
//
//  Created by msyafiqq on 28/09/20.
//  Copyright © 2020 Geniebook. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "WebAssets")!
        //webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webView.load(request)
    }

}
